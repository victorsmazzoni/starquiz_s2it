angular.module('starquiz')
    .service('QuizService', ['$http', '$q', function ($http, $q) {
        var service = {};

        service.getCharacters = function (page) {
            var deferred = $q.defer();

            $http.get('https://swapi.co/api/people/?page=' + page).then(function(response) {
                deferred.resolve(response.data);
            })['catch'](function (err) {
                deferred.reject(err);
            });

            return deferred.promise;
        };

        service.getDataByUrl = function (url) {
            var deferred = $q.defer();

            $http.get(url).then(function(response) {
                deferred.resolve(response.data);
            })['catch'](function (err) {
                deferred.reject(err);
            });

            return deferred.promise;
        };

        service.getImages = function (query) {
            var deferred = $q.defer();

            $http.get('https://www.googleapis.com/customsearch/v1?key=AIzaSyAkWIxqXT4g90lfCh6qXOy6mbsaKRCJAgw&cx=006540617622253143572%3Arkrh0cls0zy&searchType=image&q=' + query).then(function(response) {
                deferred.resolve(response.data.items);
            })['catch'](function (err) {
                deferred.reject(err);
            });

            return deferred.promise;
        };

        return service;
    }]);