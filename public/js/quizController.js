angular.module('starquiz', ['timer'])
    .controller('QuizController', ['$scope', '$q', 'QuizService', function($scope, $q, QuizService) {
        const FILL_FIELD = 'Por favor, preencha o campo antes de confirmar a resposta.';
        $scope.page = 1;
        $scope.answers = [];

        var saveData = JSON.parse(localStorage.saveData || null) || {};

        var saveLocalStorage = function (obj) {
            _.mapObject(obj, function(val, key) {
                saveData[key] = val;
            });
            saveData.time = new Date().getTime();
            localStorage.saveData = JSON.stringify(saveData);
        };

        var loadLoadLocalStorage = function () {
            return saveData || "default";
        };

        var checkAnswered = function (character) {
            var localStorage = loadLoadLocalStorage();
            return _.findWhere(localStorage.answers, {name: character.name});
        };

        var getCharactersByPage = function (page) {
            var deferred = $q.defer();
            QuizService.getCharacters(!page ? page=1 : page).then(function (data) {
                deferred.resolve(data);
            })['catch'](function (err) {
                deferred.reject(err);
            });

            return deferred.promise;
        };

        var getCharactersImageByPage = function (index) {
            var deferred = $q.defer();
            var character = $scope.characters[index];
            var answered = $scope.timeIsFinished ? $scope.timeIsFinished : checkAnswered(character);
            if (!answered) {
                QuizService.getImages(character.name).then(function (data) {
                    setImage(data, index);
                    processNextCharacter(index, deferred);
                })['catch'](function (err) {
                    deferred.reject(err);
                });
            } else {
                setAnsweredFields(character, answered);
                processNextCharacter(index, deferred);
            }

            return deferred.promise;
        };

        var processNextCharacter = function (index, deferred) {
            if (index < $scope.characters.length-1) {
                index++;
                getCharactersImageByPage(index);
            } else {
                deferred.resolve();
            }
        };

        var getPlanetDetails = function (character) {
            var deferred = $q.defer();
            QuizService.getDataByUrl(character.homeworld).then(function (data) {
                deferred.resolve(data);
            })['catch'](function (err) {
                deferred.reject(err);
            });

            return deferred.promise;
        };

        var getFilmsDetails = function (index, character) {
            var deferred = $q.defer();
            if (!promises) {
                var promises = [];
            }
            angular.forEach(character.films, function (film) {
                promises.push(getFilmDetails(index, film));
            });

            $q.all(promises).then(function(value) {
                deferred.resolve(value);
            }, function(err) {
                deferred.reject(err);
            });

            return deferred.promise;
        };

        var getFilmDetails = function (index, film) {
            var deferred = $q.defer();
            QuizService.getDataByUrl(film).then(function (data) {
                deferred.resolve(data.title);
            })['catch'](function (err) {
                deferred.reject(err);
            });

            return deferred.promise;
        };

        var getVehiclesDetails = function (index, character) {
            var deferred = $q.defer();
            if (!promises) {
                var promises = [];
            }
            angular.forEach(character.vehicles, function (vehicle) {
                promises.push(getVehicleDetails(index, vehicle));
            });

            $q.all(promises).then(function(value) {
                deferred.resolve(value);
            }, function(err) {
                deferred.reject(err);
            });

            return deferred.promise;
        };

        var getVehicleDetails = function (index, vehicle) {
            var deferred = $q.defer();
            QuizService.getDataByUrl(vehicle).then(function (data) {
                deferred.resolve(data.name);
            })['catch'](function (err) {
                deferred.reject(err);
            });

            return deferred.promise;
        };

        var getSpeciesDetails = function (index, character) {
            var deferred = $q.defer();
            if (!promises) {
                var promises = [];
            }
            angular.forEach(character.species, function (specie) {
                promises.push(getSpecieDetails(index, specie));
            });

            $q.all(promises).then(function(value) {
                deferred.resolve(value);
            }, function(err) {
                deferred.reject(err);
            });

            return deferred.promise;
        };

        var getSpecieDetails = function (index, specie) {
            var deferred = $q.defer();
            QuizService.getDataByUrl(specie).then(function (data) {
                deferred.resolve(data.name);
            })['catch'](function (err) {
                deferred.reject(err);
            });

            return deferred.promise;
        };

        var setAnsweredFields = function (character, answered) {
            character.answered = answered.answered;
            character.canShowInput = answered.canShowInput;
            character.image = answered.image;
            character.rightAnswer = answered.rightAnswer;
        };

        var setImage = function (data, index) {
            if (data && data[0] && data[0].image && data[0].image.thumbnailLink) {
                $scope.characters[index].image = data[0].image.thumbnailLink;
            }
        };

        var calculatePoints = function (character) {
            if (character.rightAnswer) {
                if (character.infoShowed) {
                    $scope.points += 5;
                } else {
                    $scope.points += 10;
                }
            }
        };

        var fillAnswers = function (characters) {
            angular.forEach(characters, function (character) {
                character.answered = true;
            });
        };

        // FillData
        $scope.getCharacters = function (page) {
            getCharactersByPage(page).then(function (data) {
                $scope.characters = data.results;
                $scope.hasPrevious = data.previous ? true : false;
                $scope.hasNext = data.next ? true : false;
                getCharactersImageByPage(0);
            })['catch'](function (err) {
                $scope.error = err;
            });
        };

        // Pagination
        $scope.previousPage = function () {
            $scope.page--;
            $scope.getCharacters($scope.page);
        };

        $scope.nextPage = function () {
            $scope.page++;
            $scope.getCharacters($scope.page);
        };

        // Answers
        $scope.validateAnswer = function (answer, character) {
            character.answered = true;
            if (answer) {
                character.rightAnswer = answer.toUpperCase().replace('-', ' ') === character.name.toUpperCase().replace('-', ' ');
                calculatePoints(character);
                $scope.answers.push(character);
                saveLocalStorage({answers: $scope.answers});
                saveLocalStorage({points: $scope.points});
                character.canShowInput = false;
            } else {
                character.answered = false;
                alert(FILL_FIELD);
            }
        };

        $scope.showInput = function (character) {
            character.canShowInput = !character.canShowInput;
        };

        // Timer
        $scope.finished = function () {
            var localStorage = loadLoadLocalStorage();
            $scope.timeIsFinished = true;
            fillAnswers($scope.characters);
            $scope.points = localStorage.points ? localStorage.points : $scope.points;

            $('#finishModal').modal({keyboard: false, backdrop: 'static'});
            $('#finishModal').modal('show');
        };

        // Modal Info
        $scope.showInfo = function (character) {
            character.infoShowed = true;
            $scope.modalDetails = character;
            getPlanetDetails(character).then(function (data) {
                $scope.modalDetails.planet = data.name;
                getFilmsDetails(0, character).then(function (data) {
                    $scope.modalDetails.filmsNames = data.toString().replace(/,/g, ', ');
                    getVehiclesDetails(0, character).then(function (data) {
                        $scope.modalDetails.vehiclesNames = data.toString().replace(/,/g, ', ');
                        getSpeciesDetails(0, character).then(function (data) {
                            $scope.modalDetails.speciesNames = data.toString().replace(/,/g, ', ');
                            $('#infoModal').modal({keyboard: false, backdrop: 'static'});
                            $('#infoModal').modal('show');
                        })['catch'](function (err) {
                            $scope.error = err;
                        });
                    })['catch'](function (err) {
                        $scope.error = err;
                    });
                })['catch'](function (err) {
                    $scope.error = err;
                });
            })['catch'](function (err) {
                $scope.error = err;
            });
        };

        // Modal User
        $scope.saveUserData = function (user) {
            var localStorage = loadLoadLocalStorage();
            localStorage.users = !_.isEmpty(localStorage.users) ? localStorage.users : [];
            var userAlreadyAnswered = _.findWhere(localStorage.users, {name: user.name});
            if ( !_.isEmpty(userAlreadyAnswered)) {
                alert('Você já salvou uma pontuação nessa sessão.');
            } else {
                user.points = $scope.points;
                localStorage.users.push(user);
                saveLocalStorage({users: localStorage.users});
                alert('Pontuação salva com sucesso.');
                $('#finishModal').modal('hide');
            }
        };

        // Init
        $scope.init = function () {
            $scope.timeIsFinished = false;
            $scope.points = 0;
            $scope.getCharacters(1);
        }
    }]);